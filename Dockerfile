FROM openjdk:20 as builder

WORKDIR /app

COPY ./build/libs/service-registry-1.0-SNAPSHOT.jar .

ENTRYPOINT java -jar ./service-registry-1.0-SNAPSHOT.jar